﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using BackupAgent.Contract;
using BackupAgent.Model;
using BackupAgent.Model.Error;
using BackupAgent.Model.Messages;

namespace AuthenticationServiceAdapter
{
    public class AuthenticationService : IAuthenticationService
    {
        private string _authenticateEndpoint = "/api/Authenticate";

        protected string AuthenticationBaseUrl
        {
            get { return ConfigurationManager.AppSettings["AuthenticationBaseUrl"]; }
        }

        public async Task<AuthenticationResponse> AuthenticateUser(AuthenticationRequest authenticationRequest)
        {
            AuthenticationResponse authenticationResponse = new AuthenticationResponse();
            try
            {
                string base64EncodedCredentials = Base64Encode(authenticationRequest.UserCredentials);


                var header = new WebHeaderCollection {{"Authorization", "Basic " + base64EncodedCredentials}};

                WebRequest request = WebRequest.Create(AuthenticationBaseUrl + _authenticateEndpoint);
                request.Method = "GET";
                request.ContentType = "application/x-www-form-urlencoded";
                request.Headers = header;

                WebResponse response = request.GetResponse();
                authenticationResponse.SecurityToken = response.Headers["Token"];

                using (var stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream);
                    string value = reader.ReadToEnd();
                }

            }
            catch (WebException ex)
            {
                authenticationResponse.Status = "Failed";
                authenticationResponse.ErrorDetails.Add(new ErrorInfo()
                {
                    ErrorCode = "AuthError01",
                    ErrorMessages = "Commmunication error occured while communicating with Authentication service."
                });
            }
            catch (InvalidOperationException ex)
            {
                authenticationResponse.Status = "Failed";
                authenticationResponse.ErrorDetails.Add(new ErrorInfo()
                {
                    ErrorCode = "AuthError02",
                    ErrorMessages = ex.Message
                });

            }
            catch (Exception ex)
            {
                authenticationResponse.Status = "Failed";
                authenticationResponse.ErrorDetails.Add(new ErrorInfo()
                {
                    ErrorCode = "SystemError01",
                    ErrorMessages = "Unable to process Authentication request."
                });
            }

            return authenticationResponse;
        }

        public static string Base64Encode(UserCredentials credentials)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(credentials.UserName+":"+credentials.Password);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}
