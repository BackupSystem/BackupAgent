﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using BackupAgent.Contract;
using BackupAgent.Model;
using System.Configuration;
using System.IO;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using BackupAgent.Model.Error;
using Newtonsoft.Json;

namespace BackupAgent.Core.Schedule
{
    public class ScheduleFacade : IScheduleFacade
    {
        protected IScheduleDataSource ScheduleDataSource { get; set; }
        protected UserCredentials UserCredentials { get; set; }

        private string _registerEndpoint= "/api/client";
        private string _scheduleEndpoint = "/api/BackupSchedules";

        protected string AuthenticationBaseUrl
        {
            get { return ConfigurationManager.AppSettings["AuthenticationBaseUrl"]; }
        }
        private IPHostEntry IPHost => System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());

        private IPAddress IpAddress => IPHost.AddressList[1];

        public ScheduleFacade(UserCredentials userCredentials,IScheduleDataSource scheduleDataSource)
        {
            ScheduleDataSource = scheduleDataSource;
            UserCredentials = userCredentials;
        }

        public List<BaseSchedule> GetSchedules()
        {
           return ScheduleDataSource.GetSchedules(IpAddress.ToString(), UserCredentials.SecurityToken);
         
        }

        public ClientRegistrationResponse Register(ClientRegistrationRequest clientRegistrationRequest)
        {
            ClientRegistrationResponse clientRegistrationResponse= new ClientRegistrationResponse();
            try
            {
                clientRegistrationRequest.IpAddress = IpAddress.ToString();
                clientRegistrationRequest.MachineName = "Rahul";// IPHost.HostName;
                clientRegistrationRequest.UserName = UserCredentials.UserName;
                clientRegistrationRequest.Password = UserCredentials.Password;

               var header = new WebHeaderCollection { { "Token", UserCredentials.SecurityToken } };
                //byte[] byteArray = null;

                //var serializer = new DataContractJsonSerializer(typeof(ClientRegistrationRequest));
                //using (var stream = new MemoryStream())
                //{
                //    serializer.WriteObject(stream, clientRegistrationRequest);
                //    byteArray = stream.ToArray();
                //}

                var parameters = Newtonsoft.Json.JsonConvert.SerializeObject(clientRegistrationRequest);
                byte[] byteArray = Encoding.UTF8.GetBytes(parameters);

                WebRequest request = WebRequest.Create(AuthenticationBaseUrl + _registerEndpoint);
                request.Method = "POST";
                request.Headers = header;
                request.ContentLength = byteArray.Length;
                request.ContentType = "application/json";

                //request.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                //request.Headers.Add(HttpRequestHeader.ContentType,"application/json");
                

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(byteArray, 0, byteArray.Length);
                requestStream.Close();

                WebResponse response = request.GetResponse();

                using (var stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream);
                    string value = reader.ReadToEnd();
                }

            }
            catch (WebException ex)
            {
                clientRegistrationResponse.Status = "Failed"; ;
                clientRegistrationResponse.ErrorDetails.Add(new ErrorInfo()
                {
                    ErrorCode = "AuthError01",
                    ErrorMessages = "Commmunication error occured while communicating with Authentication service."
                });
            }
            catch (InvalidOperationException ex)
            {
                clientRegistrationResponse.Status = "Failed";
                clientRegistrationResponse.ErrorDetails.Add(new ErrorInfo()
                {
                    ErrorCode = "AuthError02",
                    ErrorMessages = ex.Message
                });

            }
            catch (Exception ex)
            {
                clientRegistrationResponse.Status = "Failed";
                clientRegistrationResponse.ErrorDetails.Add(new ErrorInfo()
                {
                    ErrorCode = "SystemError01",
                    ErrorMessages = "Unable to process Authentication request."
                });
            }
            return clientRegistrationResponse;
        }
    }
}
