﻿using System.Threading.Tasks;
using BackupAgent.Contract;
using BackupAgent.Model;
using BackupAgent.Model.Messages;

namespace BackupAgent.Core.Authentication
{
    public class UserAuthentication : IUserAuthentication
    {
        public UserAuthentication(IAuthenticationService authenticationService)
        {
            AuthenticationService = authenticationService;
        }

        private IAuthenticationService AuthenticationService { get; }


        public async Task<AuthenticationResponse> AuthenticateUser(UserCredentials userCredentials)
        {
          var response=  await AuthenticationService.AuthenticateUser(new AuthenticationRequest()
            {
                UserCredentials = userCredentials
            });

            return response;
        }
    }
}
