﻿using System;
using System.IO;
using System.Net;
using BackupAgent.Contract;
using BackupAgent.Model;
using BackupAgent.Model.Log;
using BackupAgent.Model.Messages;
using BackupAgent.Model.NetworkAccess;

namespace BackupAgent.Core
{
    public class FileBackupExecutor : IBackupExecutor
    {
        public BackupExecutionResponse Execute(BaseSchedule backupSchedule)
        {
            BackupExecutionResponse backupExecutionResponse = new BackupExecutionResponse(backupSchedule.ScheduleId);
            FileBackupSchedule fileBackupSchedule = backupSchedule as FileBackupSchedule;
            if (fileBackupSchedule == null)
            {
                backupExecutionResponse.ActivityLogs.Logs.Add(MessageStore.FileScheduleTypeMismatch);
                return backupExecutionResponse;
            }

            //Execute the backup
           var activityLogs = ExecuteBackup(fileBackupSchedule);

            //Note down the activity log

            //Update the status 

            return backupExecutionResponse;
        }

        private ActivityLog ExecuteBackup(FileBackupSchedule fileBackupSchedule)
        {
            ActivityLog activityLog = new ActivityLog(fileBackupSchedule.ScheduleId);
            try
            {
                using (new NetworkConnection(fileBackupSchedule.SourcePath, GetSourceCredentials(fileBackupSchedule)))
                {
                    using (
                        new NetworkConnection(fileBackupSchedule.DestinationPath,
                            GetDestinationCredentials(fileBackupSchedule)))
                    {
                        activityLog.Logs.Add(MessageStore.AccessGranted);

                        //Back it up in destination
                        File.Copy(fileBackupSchedule.SourcePath, fileBackupSchedule.DestinationPath);

                        activityLog.Logs.Add(MessageStore.BackupComplete);


                        //Update the status by calling service
                        fileBackupSchedule.Status = "Completed";
                    }
                }
            }
            catch (Exception ex)
            {
                activityLog.Logs.Add(MessageStore.BackupFailed + ex.Message);
                fileBackupSchedule.Status = "Failed";
            }

            return activityLog;
        }

        private static NetworkCredential GetSourceCredentials(FileBackupSchedule fileBackupSchedule)
        {
            return new NetworkCredential(fileBackupSchedule.SourceCredentials.UserName,
                fileBackupSchedule.SourceCredentials.Password);
        }

        private static NetworkCredential GetDestinationCredentials(FileBackupSchedule fileBackupSchedule)
        {
            return new NetworkCredential(fileBackupSchedule.DestinationCredentials.UserName,
                fileBackupSchedule.DestinationCredentials.Password);
        }
    }
}
