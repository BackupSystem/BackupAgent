﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using BackupAgent.Contract;
using BackupAgent.Model;
using BackupAgent.Model.Error;
using System.Configuration;

namespace BackupAgent.ScheduleServiceAdapter
{
    public class ScheduleDataSourceService : IScheduleDataSource
    {
        private string _scheduleEndpoint = "/api/BackupSchedule";
        protected string AuthenticationBaseUrl
        {
            get { return ConfigurationManager.AppSettings["AuthenticationBaseUrl"]; }
        }

        public List<BaseSchedule> GetSchedules(string ipAddress, string token)
        {
            List <BaseSchedule> schedules=new List<BaseSchedule>();
            try
            {
               var header = new WebHeaderCollection { { "Token", token } };

                WebRequest request =
                    WebRequest.Create(AuthenticationBaseUrl + _scheduleEndpoint + "?ipAddress=" + ipAddress);// "1.1.1.1");
                request.Method = "GET";
                request.Headers = header;
                request.ContentType = "application/json";

                WebResponse response = request.GetResponse();

                using (var stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream);
                    string value = reader.ReadToEnd();
                }

            }
            catch (Exception ex)
            {
               
            }
            return schedules;
        }
    }
}
