﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BackupAgent.Core.Schedule;
using BackupAgent.MockRestService;
using BackupAgent.Model;
using BackupAgent.ScheduleServiceAdapter;
using WindowClient.ViewModels;

namespace WindowClient
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : Window
    {
        public HomeViewModel HomeViewModel { get; set; }
        public Home(UserCredentials userCredentials)
        {
            InitializeComponent();
            InitializeViewModel(userCredentials);
            this.DataContext = HomeViewModel;
        }

        private void InitializeViewModel(UserCredentials userCredentials)
        {
            HomeViewModel = new HomeViewModel(new ScheduleFacade(userCredentials,new ScheduleDataSourceService()));
            HomeViewModel.Initialize();
        }

    }
}
