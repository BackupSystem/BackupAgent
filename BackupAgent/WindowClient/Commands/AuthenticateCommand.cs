﻿using System;
using System.Runtime.Remoting.Messaging;
using System.Windows;
using System.Windows.Input;
using AuthenticationServiceAdapter;
using BackupAgent.Contract;
using BackupAgent.Core.Authentication;
using BackupAgent.MockRestService;
using BackupAgent.Model;
using BackupAgent.Model.Messages;
using WindowClient.ViewModels;

namespace WindowClient.Commands
{
    public class AuthenticateCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            //Authentication can execute for every authenticate request.
            return true;
        }

        public async void Execute(object parameter)
        {
            UserCredentials userCredentials = parameter as UserCredentials;
            if (userCredentials != null)
            {
               
                IUserAuthentication userAuthentication = new UserAuthentication(new AuthenticationService());
                AuthenticationResponse authenticationResponse = await userAuthentication.AuthenticateUser(userCredentials);

                if (authenticationResponse == null)
                {
                    MessageBox.Show("Authentication failed, please try again", "Authentication Failure",
                        MessageBoxButton.OKCancel);
                    return;
                }

                if (authenticationResponse.Status.ToLower() == "failed")
                {
                    string errorMessage = string.Empty;
                    authenticationResponse.ErrorDetails.ForEach(err => errorMessage += err.ErrorMessages);
                    MessageBox.Show(errorMessage, "Authentication Failure", MessageBoxButton.OKCancel);
                    return;
                }
                userCredentials.SecurityToken = authenticationResponse.SecurityToken;

                //Navigate to home screen
                var app = new Home(userCredentials);
                app.Show();

            }
        }
    }
}
