﻿using BackupAgent.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WindowClient.ViewModels;

namespace WindowClient.Commands
{
    public class BackupCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {

            if (parameter == null)
                return true; //Nothing is selected yet, so return true so that button is enabled

            HomeViewModel homeViewModel = parameter as HomeViewModel;
            if (homeViewModel == null)
                return false;

            if (homeViewModel.SelectedSchedule == null)
                return false;

            if (homeViewModel.SelectedSchedule.Status.ToLower() == "completed" || homeViewModel.SelectedSchedule.Status.ToLower() == "inprogress")
            {
                MessageBox.Show("Backups of Pending or Failed schedules can only be taken using this option.");
                return false;
            }

            return true;

        }

        public void Execute(object parameter)
        {
            HomeViewModel homeViewModel = parameter as HomeViewModel;
            if (homeViewModel != null)
            {
               FileBackupExecutor fileBackupExecutor = new FileBackupExecutor();
                fileBackupExecutor.Execute(homeViewModel.SelectedSchedule);
            }
        }
    }
}
