﻿using System.Windows;
using System.Windows.Controls;
using WindowClient.ViewModels;

namespace WindowClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public LoginViewModel LoginViewModel { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            InitializeViewModel();
            this.DataContext = LoginViewModel;
        }

        private void InitializeViewModel()
        {
            LoginViewModel = new LoginViewModel();
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            ((LoginViewModel) this.DataContext).User.Password = ((PasswordBox) sender).Password;

        }
    }
}
