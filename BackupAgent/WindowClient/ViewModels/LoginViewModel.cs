﻿using System.Windows.Input;
using BackupAgent.Model;
using WindowClient.Commands;

namespace WindowClient.ViewModels
{
    public class LoginViewModel
    {
        public LoginViewModel()
        {
            InitializeCommands();
            InitializeModel();
        }

        private void InitializeCommands()
        {
            AuthenticateCommand = new AuthenticateCommand();
        }

        public void InitializeModel()
        {
            User = new UserCredentials(string.Empty, string.Empty);
        }
       
        public UserCredentials User { get; set; }
        
        public ICommand AuthenticateCommand { get; set; }
        
    }
}
