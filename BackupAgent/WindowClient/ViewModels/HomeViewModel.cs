﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BackupAgent.Contract;
using BackupAgent.Model;
using WindowClient.Commands;

namespace WindowClient.ViewModels
{
   public class HomeViewModel
    {
        private IScheduleFacade ScheduleFacade { get; }
       public HomeViewModel(IScheduleFacade scheduleFacade)
       {
           ScheduleFacade = scheduleFacade;
           InitializeCommand();
       }

        private void InitializeCommand()
        {
            BackupCommand = new BackupCommand();
        }

        public void Initialize()
        {
            ScheduleFacade.Register(new ClientRegistrationRequest());
            var schedules = ScheduleFacade.GetSchedules();
            Schedules = schedules.OfType<FileBackupSchedule>().ToList();
        }
        public ICommand BackupCommand { get; set; }

        public FileBackupSchedule SelectedSchedule { get; set; }

        public List<FileBackupSchedule> Schedules { get; set; }
    }
}
