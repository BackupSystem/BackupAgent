﻿using System.ComponentModel;

namespace BackupAgent.Model
{
    public class FileBackupSchedule: BaseSchedule
    {
        public string SourcePath { get; set; }

        public string DestinationPath { get; set; }

        public UserCredentials SourceCredentials { get; set; }

        public UserCredentials DestinationCredentials { get; set; }

       
    }
}
