﻿namespace BackupAgent.Model.Error
{
    public class ErrorInfo
    {
        public string ErrorCode { get; set; }

        public string ErrorMessages { get; set; }
    }
}
