﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackupAgent.Model.Log
{
    public class ActivityLog
    {
        public ActivityLog(string scheduleId)
        {
            ScheduleId = scheduleId;
            Logs=new List<string>();
        }
        public string ScheduleId { get; set; }

        public List<string> Logs { get; set; } 
    }
}
