﻿using System.ComponentModel;

namespace BackupAgent.Model
{
    public class UserCredentials: INotifyPropertyChanged
    {

        private string _userName;
        private string _password;

        public UserCredentials(string userName, string password)
        {
            UserName = userName;
            Password = password;
        }


        public string UserName
        {
            get { return _userName;}
            set
            {
                _userName = value;
                OnPropertyChanged(nameof(UserName));
            }
        }

        public string SecurityToken { get; set; }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
