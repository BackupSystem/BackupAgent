﻿namespace BackupAgent.Model
{
    public class MessageStore
    {
        public static readonly string FileScheduleTypeMismatch = "Invalid schedule request.";

        public static readonly string BackupFailed = "Backup failed with error message - ";

        public static readonly string AccessGranted = "Access Granted on Source and Destination servers.";

        public static readonly string BackupComplete = "Backup Complete.";
    }
}
