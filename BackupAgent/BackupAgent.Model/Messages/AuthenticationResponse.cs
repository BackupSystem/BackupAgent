﻿using System.Collections.Generic;
using BackupAgent.Model.Error;

namespace BackupAgent.Model.Messages
{
    public class AuthenticationResponse
    {
        public AuthenticationResponse()
        {
            Status = "Success";
            ErrorDetails = new List<ErrorInfo>();
        }
        public string Status { get; set; }

        public string SecurityToken { get; set; }

        public List<ErrorInfo> ErrorDetails { get; set; }
    }
}
