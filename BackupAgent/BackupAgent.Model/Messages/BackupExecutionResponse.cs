﻿using System.Collections.Generic;
using BackupAgent.Model.Log;

namespace BackupAgent.Model.Messages
{
    public class BackupExecutionResponse
    {
        public BackupExecutionResponse(string scheduleId)
        {
            ActivityLogs = new ActivityLog(scheduleId);
        }

        public ActivityLog ActivityLogs { get; set; } 
    }
}
