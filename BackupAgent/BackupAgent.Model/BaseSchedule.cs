﻿using System;
using System.ComponentModel;

namespace BackupAgent.Model
{
    public abstract class BaseSchedule: INotifyPropertyChanged
    {
        private string _scheduleId;

        public string ScheduleId
        {
            get { return _scheduleId; }
            set { _scheduleId = value; OnPropertyChanged(nameof(ScheduleId)); }
        }

        public string ScheduleType { get; set; }

        public DateTime ScheduleTime { get; set; }

        private string _status;
        public string Status
        {
            get { return _status; }
            set { _status = value; OnPropertyChanged(nameof(Status)); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
