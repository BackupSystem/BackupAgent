﻿using System.Threading.Tasks;
using BackupAgent.Model;
using BackupAgent.Model.Messages;

namespace BackupAgent.Contract
{
    public interface IUserAuthentication
    {
       Task<AuthenticationResponse> AuthenticateUser(UserCredentials userCredentials);
    }
}
