﻿using BackupAgent.Model;
using BackupAgent.Model.Messages;

namespace BackupAgent.Contract
{
    public interface IBackupExecutor
    {
        BackupExecutionResponse Execute(BaseSchedule backupSchedule);
    }
}
