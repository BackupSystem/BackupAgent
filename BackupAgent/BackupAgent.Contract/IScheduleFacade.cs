﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackupAgent.Model;

namespace BackupAgent.Contract
{
    public interface IScheduleFacade
    {
        ClientRegistrationResponse Register(ClientRegistrationRequest request);
        List<BaseSchedule> GetSchedules();
    }
}
