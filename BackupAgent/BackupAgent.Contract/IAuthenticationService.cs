﻿using System.Threading.Tasks;
using BackupAgent.Model.Messages;

namespace BackupAgent.Contract
{
   public interface IAuthenticationService
    {
        Task<AuthenticationResponse> AuthenticateUser(AuthenticationRequest authenticationRequest);
    }
}
