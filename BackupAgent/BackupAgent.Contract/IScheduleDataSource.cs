﻿using System.Collections.Generic;
using BackupAgent.Model;

namespace BackupAgent.Contract
{
    public interface IScheduleDataSource
    {
        List<BaseSchedule> GetSchedules(string ipAddress, string token);
    }
}
