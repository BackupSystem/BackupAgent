﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackupAgent.Contract;
using BackupAgent.Model;

namespace BackupAgent.MockRestService
{
    public class MockScheduleDataSource : IScheduleDataSource
    {
        public List<BaseSchedule> GetSchedules(string IpAddress,string token)
        {
            return new List<BaseSchedule>()
            {
                new FileBackupSchedule()
                {
                    ScheduleId = "sc101",
                    Status = "Pending",
                    DestinationCredentials = new UserCredentials("rahul", "pawar"),
                    DestinationPath = @"D:\MyPersonal\Poc\Output",
                    ScheduleTime = DateTime.Now.AddMinutes(1),
                    ScheduleType = "FileBackup",
                    SourceCredentials = new UserCredentials("rahul", "pawar"),
                    SourcePath = @"D:\MyPersonal\Poc\Input",
                },
                new FileBackupSchedule()
                {
                    ScheduleId = "sc102",
                    Status = "Pending",
                    DestinationCredentials = new UserCredentials("rahul", "pawar"),
                    DestinationPath = @"D:\MyPersonal\Poc\Output",
                    ScheduleTime = DateTime.Now.AddMinutes(1),
                    ScheduleType = "FileBackup",
                    SourceCredentials = new UserCredentials("rahul", "pawar"),
                    SourcePath = @"D:\MyPersonal\Poc\Input",
                }
            };
        }
    }
}
