﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BackupAgent.Contract;
using BackupAgent.Model.Error;
using BackupAgent.Model.Messages;

namespace BackupAgent.MockRestService
{
    public class MockAuthenticationService : IAuthenticationService
    {
        public async Task<AuthenticationResponse> AuthenticateUser(AuthenticationRequest authenticationRequest)
        {
            if(authenticationRequest==null)
            {
                return new AuthenticationResponse()
                {
                    Status = "Failed",
                    ErrorDetails = new List<ErrorInfo>()
                    {
                        new ErrorInfo()
                        {
                            ErrorCode = "AuthError001",
                            ErrorMessages = "The Authentication Request is null."
                        }
                    }
                };
            }

            if (authenticationRequest.UserCredentials == null)
            {
                return new AuthenticationResponse()
                {
                    Status = "Failed",
                    ErrorDetails = new List<ErrorInfo>()
                    {
                        new ErrorInfo()
                        {
                            ErrorCode = "AuthError002",
                            ErrorMessages = "The User Credentials are not provided in the request."
                        }
                    }
                };
            }


            if (string.IsNullOrEmpty(authenticationRequest.UserCredentials.UserName) || 
                string.IsNullOrEmpty(authenticationRequest.UserCredentials.Password) ||
                authenticationRequest.UserCredentials.UserName.ToLower()!="rahul" ||
                authenticationRequest.UserCredentials.Password.ToLower()!="pawar")
            {
                return new AuthenticationResponse()
                {
                    Status = "Failed",
                    ErrorDetails = new List<ErrorInfo>()
                    {
                        new ErrorInfo()
                        {
                            ErrorCode = "AuthError003",
                            ErrorMessages = "Incorrect User Credentials provided in the request."
                        }
                    }
                };
            }

            return new AuthenticationResponse() {Status = "success" };
        }
    }
}
